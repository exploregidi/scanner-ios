//
//  HomePageViewController.swift
//  Gidieats-partner
//
//  Created by Olujide Jacobs on 5/22/20.
//  Copyright © 2020 Gidieats. All rights reserved.
//

import UIKit
import Alamofire

class HomePageViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let titleColor = [NSAttributedString.Key.foregroundColor: UIColor.white]
        
        navigationController?.navigationBar.titleTextAttributes = titleColor
        navigationController?.navigationBar.tintColor = .white
        navigationController?.navigationBar.barTintColor = ColorConstants.gidiGreen
        navigationController?.navigationBar.backgroundColor = ColorConstants.gidiGreen
        navigationController?.navigationBar.shadowImage = UIImage()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        fetchDeals()
        
        NotificationCenter.default.addObserver(self, selector: #selector(logoutSuccess), name: NSNotification.Name(rawValue: NOTIFICATION_REQUEST_SUCCESS), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(logoutFailure), name: NSNotification.Name(rawValue: NOTIFICATION_REQUEST_FAILURE), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(serverError), name: NSNotification.Name(rawValue: NOTIFICATION_SERVER_ERROR), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(expiredToken), name: NSNotification.Name(rawValue: NOTIFICATION_EXPIRED_TOKEN), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(forbiddenRequest), name: NSNotification.Name(rawValue: NOTIFICATION_FORBIDDEN_REQUEST), object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFICATION_REQUEST_SUCCESS), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFICATION_REQUEST_FAILURE), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFICATION_SERVER_ERROR), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFICATION_EXPIRED_TOKEN), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFICATION_FORBIDDEN_REQUEST), object: nil)
    }
    
    var deal: [Deal]? = [] {
        didSet {
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        }
    }
    
    var message: String?
    
    let statusBar =  UIView()
    
    let refresh = UIRefreshControl()
    
    let homeRowId = "homeRowId"
    
    let textLabel: AuthLabel = {
        let label = AuthLabel()
        label.textColor = ColorConstants.gidiGreen
        label.textAlignment = .center
        label.text = "No deals"
        label.numberOfLines = Constants.defaultLabelLine
        return label
    }()
    
    let purchaseDealButton: AuthButton = {
        let button = AuthButton()
        button.setTitle("Refresh", for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: Constants.smallerButtonFont, weight: .medium)
        button.titleLabel?.font = UIFont(name: StringConstants.defaultFont, size: Constants.buttonFont)
        button.layer.cornerRadius = Constants.buttonCornerRadius
        button.backgroundColor = ColorConstants.gidiGray
        button.addTarget(self, action: #selector(fetchDeals), for: .touchUpInside)
        return button
    }()
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return deal?.count ?? 0
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: homeRowId, for: indexPath) as! GenericCollectionViewCell
        cell.deal = deal?[indexPath.item]
        
        if let expiryStatus = deal?[indexPath.item].isExpired {
            var expiryString: Bool?
            
            expiryString = expiryStatus
            
            cell.expiryLabel.text = nil
            cell.expiryLabel.layer.backgroundColor = nil
            
            if expiryStatus == true {
                if expiryStatus == expiryString {
                    DispatchQueue.main.async {
                        cell.expiryLabel.layer.backgroundColor = UIColor.black.cgColor
                        cell.expiryLabel.layer.cornerRadius = Constants.tableHeightSpacingOffest
                        cell.expiryLabel.text = "Expired"
                        cell.addSubview(cell.expiryLabel)
                        _ = cell.expiryLabel.anchor(cell.imageView.topAnchor, right: cell.imageView.trailingAnchor, topConstant: Constants.minConstraintOffset, rightConstant: Constants.minConstraintOffset, widthConstant: Constants.buttonWidth, heightConstant: Constants.indicatorCornerRadius)
                    }
                }
            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if UIDevice.current.userInterfaceIdiom == .pad {
            return CGSize(width: view.frame.width, height: Constants.collectionCellHeightForPad)
        } else {
            return CGSize(width: view.frame.width, height: Constants.collectionCellHeight)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        UserDefaults.standard.set(deal?[indexPath.item].slug, forKey: APIConstants.dealSlug)
        pushViewController(viewController: DealDetailsViewController())
    }
    
    @objc func fetchDeals() {
        self.activityIndicatorStart()
        self.networkLoaderStart()
        
        let token = UserDefaults.standard.object(forKey: APIConstants.token) as? String ?? APIConstants.emptyValue
        let userId = UserDefaults.standard.object(forKey: APIConstants.userId) as? String ?? APIConstants.emptyValue
        let urlString = APIConstants.baseURL + APIConstants.users + userId
        guard let urlRequest = URL(string: urlString) else { return }
        var request = URLRequest(url: urlRequest)
        request.httpMethod = APIConstants.get
        request.addValue(APIConstants.applicationJson, forHTTPHeaderField: APIConstants.contentType)
        request.addValue(APIConstants.bearer + token, forHTTPHeaderField: APIConstants.authorization)
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error != nil {
                _ = error
                return
            }
            do {
                let jsonData = try JSONDecoder().decode(UserData.self, from: data!)
                UserDefaults.standard.set(jsonData.data.restaurant.slug, forKey: APIConstants.restaurantSlug)
                let restaurantSlug = jsonData.data.restaurant.slug ?? APIConstants.emptyValue
                
                if Connectivity.isConnected() {
                    let urlString = APIConstants.baseURL + APIConstants.deals + "\(APIConstants.itemsPerPage)\(Constants.defaultItems)&restaurantSlug=\(restaurantSlug)"
                    guard let url = URL(string: urlString) else { return }
                    URLSession.shared.dataTask(with: url) { (data, response, error) in
                        if error != nil {
                            _ = error
                            return
                        }
                        do {
                            let jsonData = try JSONDecoder().decode(DealData.self, from: data!)
                            
                            self.deal = [Deal]()
                            
                            for dictionary in jsonData.data {
                                let section = Deal(slug: dictionary.slug, name: dictionary.name, percentOff: dictionary.percentOff, imageUrl: dictionary.imageUrl, timeCreated: dictionary.timeCreated, expiryDate: dictionary.expiryDate, isExpired: dictionary.isExpired, restaurant: dictionary.restaurant)
                                self.deal?.append(section)
                            }
                            self.networkLoaderStop()
                            self.activityIndicatorStop()
                            
                            DispatchQueue.main.async {
                                if self.deal?.count ?? 0 == 0 {
                                    self.addEmptyDealPrompt()
                                } else {
                                    self.removeEmptyDealPrompt()
                                }
                            }
                        } catch _ {
                            let response = response.debugDescription
                            if response.contains(APIConstants.unauthorizedResponse) {
                                AuthService.newToken()
                                
                                let time = DispatchTime.now() + Constants.tokenTimer
                                DispatchQueue.main.asyncAfter(deadline: time) {
                                    self.fetchDeals()
                                }
                            } else if response.contains(APIConstants.badResponse) {
                                UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.token)
                                UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.refreshToken)
                                UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.userId)
                                UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.restaurantSlug)
                                
                                self.alertAndExit(message: APIConstants.reauthorize, title: APIConstants.unauthorized)
                            } else {
                                DispatchQueue.main.async {
                                    self.internetCheck(text: APIConstants.checkConnection)
                                    self.networkLoaderStop()
                                    self.activityIndicatorStop()
                                }
                            }
                        }
                    }.resume()
                } else {
                    DispatchQueue.main.async {
                        self.internetCheck(text: APIConstants.checkConnection)
                        self.networkLoaderStop()
                        self.activityIndicatorStop()
                    }
                }
                self.networkLoaderStop()
                self.activityIndicatorStop()
            } catch _ {
                let response = response.debugDescription
                if response.contains(APIConstants.unauthorizedResponse) {
                    AuthService.newToken()
                    
                    let time = DispatchTime.now() + Constants.tokenTimer
                    DispatchQueue.main.asyncAfter(deadline: time) {
                        self.fetchDeals()
                    }
                } else if response.contains(APIConstants.badResponse) {
                    UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.token)
                    UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.refreshToken)
                    UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.userId)
                    UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.restaurantSlug)
                    
                    self.alertAndExit(message: APIConstants.reauthorize, title: APIConstants.unauthorized)
                } else {
                    DispatchQueue.main.async {
                        self.internetCheck(text: APIConstants.checkConnection)
                        self.networkLoaderStop()
                        self.activityIndicatorStop()
                    }
                }
            }
        }.resume()
    }
    
    func addEmptyDealPrompt() {
        textLabel.isHidden = false
        purchaseDealButton.isHidden = false
        
        view.addSubview(textLabel)
        view.addSubview(purchaseDealButton)
        _ = textLabel.anchor(left: view.safeAreaLayoutGuide.leadingAnchor, right: view.safeAreaLayoutGuide.trailingAnchor, centerY: view.centerYAnchor, leftConstant: Constants.sideConstraintForLabel, rightConstant: Constants.sideConstraintForLabel)
        _ = purchaseDealButton.anchor(textLabel.bottomAnchor, centerX: textLabel.centerXAnchor, topConstant: Constants.topConstraintBetweenLabels, widthConstant: Constants.textFieldWidth)
    }
    
    func removeEmptyDealPrompt() {
        textLabel.isHidden = true
        purchaseDealButton.isHidden = true
    }
    
    private func configureUI() {
        collectionView.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        
        statusBar.frame = UIApplication.shared.statusBarFrame
        statusBar.backgroundColor = ColorConstants.gidiGreen
        view.addSubview(statusBar)
        
        refresh.addTarget(self, action: #selector(reloadApp), for: .valueChanged)
        
        navigationController?.navigationBar.prefersLargeTitles = true
                
        navigationItem.title = "All Deals"
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Redeem", style: .plain, target: self, action: #selector(redeemDeal))
        navigationItem.rightBarButtonItems = [UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(logoutUser)), UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addDeal))]
        
        collectionView.register(GenericCollectionViewCell.self, forCellWithReuseIdentifier: homeRowId)
        collectionView.refreshControl = refresh
    }
    
    @objc func reloadApp() {
        fetchDeals()
        refresh.endRefreshing()
    }
    
    @objc func addDeal() {
        presentViewController(viewController: UINavigationController(rootViewController: AddDealsViewController()))
    }
    
    @objc func redeemDeal() {
        pushViewController(viewController: QRCodeScannerViewController())
    }
    
    @objc func logoutUser() {
        let token = UserDefaults.standard.object(forKey: APIConstants.token) as? String ?? APIConstants.emptyValue
        let refreshToken = UserDefaults.standard.object(forKey: APIConstants.refreshToken) as? String ?? APIConstants.emptyValue
        
        if Connectivity.isConnected() {
            AuthService.postRequest(urlString: APIConstants.baseURL + APIConstants.account + "logout", headers: [APIConstants.contentType: APIConstants.applicationJson, APIConstants.authorization: APIConstants.bearer + token], parameters: [
                APIConstants.refreshToken: refreshToken
            ])
        } else {
            self.alert(message: APIConstants.noConnection, title: APIConstants.connectionError)
        }
    }
    
    @objc func logoutSuccess() {
        switchView(value: false)
    }
    
    @objc func logoutFailure() {
        message = AuthService.message
        self.alert(message: message ?? APIConstants.emptyValue, title: StringConstants.error)
    }
    
    @objc func serverError() {
        message = AuthService.message
        self.alert(message: message ?? APIConstants.emptyValue, title: StringConstants.error)
    }
    
    @objc func expiredToken() {
        AuthService.newToken()
        
        let time = DispatchTime.now() + Constants.tokenTimer
        DispatchQueue.main.asyncAfter(deadline: time) {
            self.logoutUser()
        }
    }
    
    @objc func forbiddenRequest() {
        self.alertAndExit(message: APIConstants.reauthorize, title: APIConstants.unauthorized)
    }
    
}
        
        
//        self.networkLoaderStart()
//        self.view.isUserInteractionEnabled = false
//        self.activityIndicatorStart()
//
//        let parameters: [String: Any] = [
//            APIConstants.refreshToken: refreshToken
//        ]
        
//        let headers: HTTPHeaders = [APIConstants.contentType: APIConstants.applicationJson,
//                                    APIConstants.authorization: APIConstants.bearer + token
//        ]
        
//        AF.request(APIConstants.baseURL + APIConstants.account + "logout", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
//
//            if Connectivity.isConnected() { //Check connectivity status
//                let info = serializeResponse(data: response.data ?? Data())
//                switch response.result {
//                case .success(_):
//                    if info[APIConstants.isSuccess] as? Bool ?? false == true {
//                        if info[APIConstants.data] != nil {
//                            UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.token)
//                            UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.refreshToken)
//                            UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.userId)
//                            UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.restaurantSlug)
                            
//                            self.switchView(value: false)
//                        }
//                    } else {
//                        let userData = info[APIConstants.errorMessages] as? [String] ?? [APIConstants.somethingWrong]
//                        let message = userData.joined()
//                        self.alert(message: message, title: StringConstants.error)
//                    }
//                case .failure(_):
//                    let response = response.debugDescription
//                    if response.contains(APIConstants.unauthorizedResponse) {
//                        AuthService.newToken()
//
//                        let time = DispatchTime.now() + Constants.tokenTimer
//                        DispatchQueue.main.asyncAfter(deadline: time) {
//                            self.logoutUser()
//                        }
//                    } else if response.contains(APIConstants.badResponse) {
//                        UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.token)
//                        UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.refreshToken)
//                        UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.userId)
//                        UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.restaurantSlug)
//
//                        self.alertAndExit(message: APIConstants.reauthorize, title: APIConstants.unauthorized)
//                    } else {
//                        DispatchQueue.main.async {
//                            self.alert(message: APIConstants.somethingWrong, title: APIConstants.serverError)
//                        }
//                    }
//                }
//            } else {
//                self.alert(message: APIConstants.noConnection, title: APIConstants.connectionError)
//            }
//            self.activityIndicatorStop()
//            self.view.isUserInteractionEnabled = true
//            self.networkLoaderStop()
//        }
//    }
