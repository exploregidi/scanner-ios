//
//  LoginViewController.swift
//  Gidieats
//
//  Created by Olujide Jacobs on 3/9/20.
//  Copyright © 2020 Gidieats. All rights reserved.
//

import UIKit
import Alamofire

class LoginViewController: UIViewController, UITextFieldDelegate {
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.emailField.text = UserDefaults.standard.object(forKey: APIConstants.emailAddress) as? String ?? APIConstants.emptyValue //Reset to most current email for the textfield
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        
        NotificationCenter.default.addObserver(self, selector: #selector(loginSuccess), name: NSNotification.Name(rawValue: NOTIFICATION_REQUEST_SUCCESS), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(loginFailure), name: NSNotification.Name(rawValue: NOTIFICATION_REQUEST_FAILURE), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(serverError), name: NSNotification.Name(rawValue: NOTIFICATION_SERVER_ERROR), object: nil)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFICATION_REQUEST_SUCCESS), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFICATION_REQUEST_FAILURE), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFICATION_SERVER_ERROR), object: nil)
    }
    
    var message: String?
    
    let shadowView = UIView()
    
    let backgroundImage: UIImageView = {
        let image = UIImageView()
        image.contentMode = .scaleAspectFill
        image.image = UIImage(named: StringConstants.lekki)
        image.alpha = Constants.buttonAlpha
        return image
    }()
    
    let emailField: AuthTextField = {
        let field = AuthTextField()
        field.attributedPlaceholder = NSAttributedString(string: StringConstants.email, attributes: [NSAttributedString.Key.foregroundColor: ColorConstants.gidiGray])
        field.keyboardType = .emailAddress
        field.autocorrectionType = .no
        field.returnKeyType = .next
        if #available(iOS 12.0, *) {
            field.textContentType = UITextContentType.oneTimeCode
        } else {
            field.textContentType = .init(rawValue: APIConstants.emptyValue)
        }
        return field
    }()
    
    let passwordField: AuthTextField = {
        let field = AuthTextField()
        field.attributedPlaceholder = NSAttributedString(string: StringConstants.password, attributes: [NSAttributedString.Key.foregroundColor: ColorConstants.gidiGray])
        field.isSecureTextEntry = true
        field.returnKeyType = .go
        if #available(iOS 12.0, *) {
            field.textContentType = UITextContentType.oneTimeCode
        } else {
            field.textContentType = .init(rawValue: APIConstants.emptyValue)
        }
        return field
    }()
    
    let toggleButton: AuthButton = {
        let button = AuthButton()
        button.setTitle("SHOW", for: .normal)
        button.setTitleColor(ColorConstants.gidiGray, for: .normal)
        button.layer.backgroundColor = UIColor.white.cgColor
        button.layer.cornerRadius = Constants.indicatorValue
        button.titleLabel?.font = UIFont(name: StringConstants.defaultFont, size: Constants.smallerButtonFont)
        button.addTarget(self, action: #selector(showPassword), for: .touchUpInside)
        return button
    }()
    
    let submitButton: AuthButton = {
        let button = AuthButton()
        button.setTitle(StringConstants.submit, for: .normal)
        button.titleLabel?.font = UIFont(name: StringConstants.defaultFont, size: Constants.normalLabelFont)
        button.backgroundColor = ColorConstants.gidiGreen
        button.layer.cornerRadius = Constants.buttonCorner
        button.addTarget(self, action: #selector(submitForm), for: .touchUpInside)
        return button
    }()
    
    private func configureUI() {        
        view.backgroundColor = .white
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.tintColor = ColorConstants.gidiGreen
        
        navigationItem.title = "Log in"
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(closePage))
        
        shadowView.translateAll()
        shadowView.backgroundColor = UIColor.white
        shadowView.layer.cornerRadius = Constants.texFieldHeightOffset
        shadowView.layer.shadowColor = UIColor.lightGray.cgColor
        shadowView.layer.borderColor = UIColor.lightGray.cgColor
        shadowView.layer.shadowOpacity = Constants.shadowOpacity
        
        emailField.delegate = self
        passwordField.delegate = self
        
        view.addSubview(backgroundImage)
        view.addSubview(shadowView)
        view.addSubview(emailField)
        view.addSubview(passwordField)
        view.addSubview(toggleButton)
        view.addSubview(submitButton)
        
        _ = backgroundImage.anchor(view.topAnchor, left: view.leadingAnchor, bottom: view.bottomAnchor, right: view.trailingAnchor)
        _ = shadowView.anchor(view.safeAreaLayoutGuide.topAnchor, left: view.safeAreaLayoutGuide.leadingAnchor, right: view.safeAreaLayoutGuide.trailingAnchor, topConstant: Constants.topConstraintForTextField, leftConstant: Constants.leadingConstraintForTextField, rightConstant: Constants.trailingConstraintForTextField, heightConstant: Constants.shadowHeight)
        _ = emailField.anchor(shadowView.safeAreaLayoutGuide.topAnchor, left: shadowView.safeAreaLayoutGuide.leadingAnchor, right: shadowView.safeAreaLayoutGuide.trailingAnchor, topConstant: Constants.topConstraintForTextField, leftConstant: Constants.spacingBetweenTextFields, rightConstant: Constants.spacingBetweenTextFields, heightConstant: Constants.textFieldHeight)
        _ = passwordField.anchor(emailField.bottomAnchor, left: emailField.leadingAnchor, right: emailField.trailingAnchor, topConstant: Constants.topConstraintForTextField, rightConstant: Constants.indicatorWidth, heightConstant: Constants.textFieldHeight)
        _ = toggleButton.anchor(left: passwordField.trailingAnchor, centerY: passwordField.centerYAnchor, widthConstant: Constants.indicatorWidth)
        _ = submitButton.anchor(passwordField.bottomAnchor, left: passwordField.leadingAnchor, right: emailField.trailingAnchor, topConstant: Constants.topConstraintForTextField, heightConstant: Constants.textFieldHeight)
    }
    
    @objc func closePage() {
        dismiss(animated: true, completion: nil)
    }
    
    var buttonClick = false
    
    @objc func showPassword() {
        buttonClick = !buttonClick
        if buttonClick == false {
            toggleButton.setTitle("SHOW", for: .normal)
            passwordField.isSecureTextEntry = true
        } else {
            toggleButton.setTitle("HIDE", for: .normal)
            passwordField.isSecureTextEntry = false
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == emailField {
            passwordField.becomeFirstResponder()
        } else {
            submitForm()
        }
        return true
    }
    
    @objc func submitForm() {
        view.endEditing(true)
        
        let email = emailField.text!
        let password = passwordField.text!
        
        if (email.isEmpty) {
            emailField.attributedPlaceholder = NSAttributedString(string: StringConstants.email, attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
        } else if (password.isEmpty) {
            passwordField.attributedPlaceholder = NSAttributedString(string: StringConstants.password, attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
        } else if (email.isValidEmail() == false) {
            self.alert(message: StringConstants.invalidEmail, title: StringConstants.error)
        } else {
            if Connectivity.isConnected() { //Check connectivity status
                self.submitButton.layer.backgroundColor = UIColor.lightGray.cgColor
                self.submitButton.setTitle("", for: .normal)
                self.submitButton.addSpinner()
                AuthService.postRequest(urlString: APIConstants.baseURL + APIConstants.account + "login", headers: [APIConstants.contentType: APIConstants.applicationJson], parameters: [
                    APIConstants.email: email,
                    APIConstants.password: password
                ])
            } else {
                self.alert(message: APIConstants.noConnection, title: APIConstants.connectionError)
            }
        }
    }
    
    @objc func loginSuccess() {
        self.submitButton.removeSpinner()
        self.submitButton.layer.backgroundColor = ColorConstants.gidiGreen.cgColor
        self.submitButton.setTitle(StringConstants.submit, for: .normal)
        switchView(value: true)
    }
    
    @objc func loginFailure() {
        self.submitButton.removeSpinner()
        self.submitButton.layer.backgroundColor = ColorConstants.gidiGreen.cgColor
        self.submitButton.setTitle(StringConstants.submit, for: .normal)
        message = AuthService.message
        self.alert(message: message ?? APIConstants.emptyValue, title: StringConstants.error)
    }
    
    @objc func serverError() {
        self.submitButton.removeSpinner()
        self.submitButton.layer.backgroundColor = ColorConstants.gidiGreen.cgColor
        self.submitButton.setTitle(StringConstants.submit, for: .normal)
        message = AuthService.message
        self.alert(message: message ?? APIConstants.emptyValue, title: StringConstants.error)
    }
    
}
