//
//  AppDelegate.swift
//  Gidieats-scanner
//
//  Created by Olujide Jacobs on 7/8/20.
//  Copyright © 2020 jidejakes. All rights reserved.
//

import UIKit
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

        var window: UIWindow?
        
        func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
            
//            FirebaseApp.configure()
            
            UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffset(horizontal: -1000.0, vertical: 0.0), for: .default)
            
            Thread.sleep(forTimeInterval: TimeInterval(Constants.appLoadingTime))
                        
            window = UIWindow(frame: UIScreen.main.bounds)
            window?.makeKeyAndVisible()
            window?.backgroundColor = .white
            
            let welcomeVc = WelcomePageViewController()
            let status = UserDefaults.standard.bool(forKey: StringConstants.status)
            
            if status {
                ViewSwitcher.updateRootVC()
            } else {
                window?.rootViewController = welcomeVc //Set our root view to the welcome page
            }
            return true
        }
    }
