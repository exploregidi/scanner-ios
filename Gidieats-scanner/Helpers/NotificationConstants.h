//
//  NotificationConstants.h
//  AxoCheck
//
//  Created by Olujide Jacobs on 8/24/20.
//  Copyright © 2020 jidejakes. All rights reserved.
//

#define NOTIFICATION_REQUEST_SUCCESS    @"NotificationRequestSuccess"
#define NOTIFICATION_REQUEST_FAILURE    @"NotificationRequestFailure"
#define NOTIFICATION_SERVER_ERROR       @"NotificationServerError"
#define NOTIFICATION_EXPIRED_TOKEN      @"NotificationExpiredToken"
#define NOTIFICATION_FORBIDDEN_REQUEST  @"NotificationForbiddenRequest"
