//
//  ConstraintExtension.swift
//  Gidieats
//
//  Created by Olujide Jacobs on 8/17/19.
//  Copyright © 2019 Gidieats. All rights reserved.
//

import UIKit

struct Activity {
    static let indicator = UIActivityIndicatorView(frame: CGRect(x: Constants.indicatorValue, y: Constants.indicatorValue, width: Constants.indicatorWidth, height: Constants.indicatorHeight))
    static let overlay = UIView()
}

//Helper for constraints setup
extension UIView {
    func translateAll() {
        self.translatesAutoresizingMaskIntoConstraints = false
    }
    
    func anchor(_ top: NSLayoutYAxisAnchor? = nil, left: NSLayoutXAxisAnchor? = nil, bottom: NSLayoutYAxisAnchor? = nil, right: NSLayoutXAxisAnchor? = nil, centerX: NSLayoutXAxisAnchor? = nil, centerY: NSLayoutYAxisAnchor? = nil, topConstant: CGFloat = 0, leftConstant: CGFloat = 0, bottomConstant: CGFloat = 0, rightConstant: CGFloat = 0, widthConstant: CGFloat = 0, heightConstant: CGFloat = 0, widthMultiplier:  CGFloat = 0, heightMultiplier: CGFloat = 0, centerXConstant: CGFloat = 0, centerYConstant: CGFloat = 0) -> [NSLayoutConstraint] {
        
        translateAll()
        
        var anchors = [NSLayoutConstraint]()
        
        if let top = top {
            anchors.append(topAnchor.constraint(equalTo: top, constant: topConstant))
        }
        
        if let left = left {
            anchors.append(leadingAnchor.constraint(equalTo: left, constant: leftConstant))
        }
        
        if let bottom = bottom {
            anchors.append(bottomAnchor.constraint(equalTo: bottom, constant: bottomConstant))
        }
        
        if let right = right {
            anchors.append(trailingAnchor.constraint(equalTo: right, constant: -rightConstant))
        }
        
        if let centerX = centerX {
            anchors.append(centerXAnchor.constraint(equalTo: centerX, constant: centerXConstant))
        }
        
        if let centerY = centerY {
            anchors.append(centerYAnchor.constraint(equalTo: centerY, constant: -centerYConstant))
        }
        
        if widthMultiplier > 0 {
            anchors.append(widthAnchor.constraint(equalTo: (superview?.widthAnchor)!, multiplier: widthMultiplier))
        }
        
        if heightMultiplier > 0 {
            anchors.append(heightAnchor.constraint(equalTo: (superview?.heightAnchor)!, multiplier: heightMultiplier))
        }
        
        if widthConstant > 0 {
            anchors.append(widthAnchor.constraint(equalToConstant: widthConstant))
        }
        
        if heightConstant > 0 {
            anchors.append(heightAnchor.constraint(equalToConstant: heightConstant))
        }
        
        anchors.forEach({$0.isActive = true})
        return anchors
    }
    
    func viewContainer() {
        self.backgroundColor = .clear
        self.translateAll()
    }
    
    func line() {
        self.backgroundColor = .white
        self.translateAll()
    }
    
    func addConstraintsWithFormat(format: String, views: UIView...) {
        var viewsDictionary = [String: UIView]()
        for (index, view) in views.enumerated() {
            let key = "v\(index)"
            view.translateAll()
            viewsDictionary[key] = view
        }
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutConstraint.FormatOptions(), metrics: nil, views: viewsDictionary))
    }
    
    func setUpStatusBar() {
        let statusBar =  UIView()
        
        statusBar.frame = UIApplication.shared.statusBarFrame
        statusBar.backgroundColor = ColorConstants.gidiGreen
        self.addSubview(statusBar)
    }
    
    func setUpDropdown() {
        let dropDown: UIImageView = {
            let image = UIImageView()
            image.image = UIImage(named: "dropdown")
            image.alpha = 0.5
            return image
        }()
        
        self.sizeToFit()
        self.tintColor = .clear
        
        self.addSubview(dropDown)
        
        _ = dropDown.anchor(right: self.trailingAnchor, centerY: self.centerYAnchor, rightConstant: 8)
    }
    
    func addSpinner() {
        DispatchQueue.main.async {
            
            self.addSubview(Activity.indicator)
            _ = Activity.indicator.anchor(centerX: self.centerXAnchor, centerY: self.centerYAnchor)
            
            Activity.indicator.tintColor = ColorConstants.gidiGreen
            Activity.indicator.hidesWhenStopped = true
            Activity.indicator.style = UIActivityIndicatorView.Style.gray
            Activity.indicator.startAnimating()
        }
    }
    
    func removeSpinner() {
        DispatchQueue.main.async {
            Activity.indicator.stopAnimating()
        }
    }
}

extension UIViewController {
    func alert(message: String, title: String) {
        DispatchQueue.main.async {
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            let OKAction = UIAlertAction(title: StringConstants.okay, style: .default, handler: nil)
            alertController.addAction(OKAction)
            alertController.view.tintColor = ColorConstants.gidiGreen
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func alertAndDismissToRoot(message: String, title: String) {
        DispatchQueue.main.async {
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            let OKAction = UIAlertAction(title: StringConstants.okay, style: .default, handler: { action in self.presentingViewController?.presentingViewController?.dismiss(animated: false, completion: nil)})
            alertController.addAction(OKAction)
            alertController.view.tintColor = ColorConstants.gidiGreen
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func alertAndDismiss(message: String, title: String) {
        DispatchQueue.main.async {
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            let OKAction = UIAlertAction(title: StringConstants.okay, style: .default, handler: { action in self.dismiss(animated: true, completion: nil)})
            alertController.addAction(OKAction)
            alertController.view.tintColor = ColorConstants.gidiGreen
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func alertAndExit(message: String, title: String) {
        DispatchQueue.main.async {
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            let OKAction = UIAlertAction(title: StringConstants.okay, style: .default, handler: { action in self.switchView(value: false)})
            alertController.addAction(OKAction)
            alertController.view.tintColor = ColorConstants.gidiGreen
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func alertAndPop(message: String, title: String) {
        DispatchQueue.main.async {
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            let OKAction = UIAlertAction(title: StringConstants.okay, style: .default, handler: { action in self.navigationController?.popViewController(animated: true)
            })
            alertController.addAction(OKAction)
            alertController.view.tintColor = ColorConstants.gidiGreen
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func activityIndicatorStart() {
        DispatchQueue.main.async {
            Activity.overlay.isHidden = false
            Activity.overlay.translateAll()
            Activity.overlay.layer.backgroundColor = UIColor.white.cgColor
            Activity.overlay.layer.cornerRadius = Constants.buttonCorner
            
            self.view.addSubview(Activity.overlay)
            _ = Activity.overlay.anchor(centerX: self.view.centerXAnchor, centerY: self.view.centerYAnchor, widthConstant: Constants.frameInset, heightConstant: Constants.frameInset)
            
            Activity.indicator.hidesWhenStopped = true
            Activity.indicator.style = UIActivityIndicatorView.Style.gray
            Activity.indicator.startAnimating()
            
            Activity.overlay.addSubview(Activity.indicator)
            _ = Activity.indicator.anchor(centerX: Activity.overlay.centerXAnchor, centerY: Activity.overlay.centerYAnchor)
        }
    }
    
    func activityIndicatorStop() {
        DispatchQueue.main.async {
            Activity.overlay.isHidden = true
            Activity.indicator.stopAnimating()
        }
    }
    
    func networkLoaderStart() {
        DispatchQueue.main.async {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
    }
    
    func networkLoaderStop() {
        DispatchQueue.main.async {
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
        }
    }
    
    func pushViewController(viewController: UIViewController) {
        let vc = viewController
        self.navigationController?.pushViewController(vc, animated: true)
        return
    }
    
    func presentViewController(viewController: UIViewController) {
        let vc = viewController
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
        return
    }
    
    func switchView(value: Bool, key: String = StringConstants.status) {
        UserDefaults.standard.set(value, forKey: key)
        ViewSwitcher.updateRootVC()
    }
    
    func internetCheck(text: String) {
        let textLabel: AuthLabel = {
            let label = AuthLabel()
            label.textColor = ColorConstants.gidiGreen
            label.textAlignment = .center
            label.text = text
            label.numberOfLines = Constants.defaultLabelLine
            return label
        }()
        
        let refreshButton: AuthButton = {
            let button = AuthButton()
            button.setTitle("Refresh", for: .normal)
            button.setTitleColor(.black, for: .normal)
            button.titleLabel?.font = UIFont.systemFont(ofSize: Constants.smallerButtonFont, weight: .medium)
            button.titleLabel?.font = UIFont(name: StringConstants.defaultFont, size: Constants.buttonFont)
            button.layer.cornerRadius = Constants.buttonCornerRadius
            button.backgroundColor = ColorConstants.gidiGray
            button.addTarget(self, action: #selector(refresh), for: .touchUpInside)
            return button
        }()
        
        self.view.addSubview(textLabel)
        self.view.addSubview(refreshButton)
        _ = textLabel.anchor(left: self.view.safeAreaLayoutGuide.leadingAnchor, right: self.view.safeAreaLayoutGuide.trailingAnchor, centerY: self.view.centerYAnchor, leftConstant: Constants.sideConstraintForLabel, rightConstant: Constants.sideConstraintForLabel)
        _ = refreshButton.anchor(textLabel.bottomAnchor, centerX: textLabel.centerXAnchor, topConstant: Constants.topConstraintBetweenLabels, widthConstant: Constants.textFieldWidth)
    }
    
    @objc func refresh() {
        switchView(value: true)
    }
    
    struct LoadingView {
        static let textLabel: AuthLabel = {
            let label = AuthLabel()
            label.textAlignment = .center
            label.textColor = .white
            label.text = "Loading..."
            label.layer.backgroundColor = UIColor.lightGray.withAlphaComponent(Constants.labelBackgroundAlpha).cgColor
            label.layer.cornerRadius = Constants.buttonDivider
            label.font = UIFont.systemFont(ofSize: Constants.normalLabelFont, weight: .regular)
            label.numberOfLines = Constants.defaultLabelLine
            return label
        }()
    }
    
    func showLoadingView() {
        DispatchQueue.main.async {
            LoadingView.textLabel.isHidden = false
            self.view.addSubview(LoadingView.textLabel)
            _ = LoadingView.textLabel.anchor(centerX: self.view.centerXAnchor, centerY: self.view.centerYAnchor, widthConstant: Constants.labelBorderWidth, heightConstant: Constants.labelBorderHeight)
        }
    }
    
    func hideLoadingView() {
        DispatchQueue.main.async {
            LoadingView.textLabel.isHidden = true
        }
    }
}

extension UIDevice {
    var hasNotch: Bool {
        let bottom = UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0
        return bottom > 0
    }
}

extension String {
    func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: self)
    }
    
    func isValidPassword() -> Bool {
        let validPassword = self.count >= Constants.minPassword
        return (validPassword)
    }
    
    func capitalizingFirstLetter() -> String {
        return prefix(1).uppercased() + self.lowercased().dropFirst()
    }
}

extension UserDefaults {
    private static var index = 0
    static func createCleanForTest(label: StaticString = #file) -> UserDefaults {
        index += 1
        let suiteName = "UnitTest-UserDefaults-\(label)-\(index)"
        UserDefaults().removePersistentDomain(forName: suiteName)
        return UserDefaults(suiteName: suiteName)!
    }
}

class CustomImageView: UIImageView {
    
    var imageCache = NSCache<AnyObject, AnyObject>()
    
    var imageUrlString: String?
    
    func load(urlString: String) {
        
        imageCache = NSCache()
        
        imageUrlString = urlString
        
        let url = URL(string: urlString)
        
        if let imageFromCache = imageCache.object(forKey: urlString as AnyObject) as? UIImage {
            self.image = imageFromCache
            return
        }
        
        URLSession.shared.dataTask(with: url ?? URL(fileURLWithPath: urlString), completionHandler:
            { (data, response, error) in
                if error != nil {
                    print(error!)
                    return
                }
                
                DispatchQueue.global().async {
                    if let imageToCache = UIImage(data: data!) {
                        if self.imageUrlString == urlString {
                            DispatchQueue.main.async {
                                self.image = imageToCache
                                self.imageCache.setObject(imageToCache, forKey: urlString as AnyObject)
                            }
                        }
                    }
                }
        }).resume()
    }
}
