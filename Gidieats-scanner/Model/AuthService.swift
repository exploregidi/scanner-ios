//
//  RefreshToken.swift
//  Gidieats
//
//  Created by Olujide Jacobs on 5/9/20.
//  Copyright © 2020 Gidieats. All rights reserved.
//

import Foundation
import Alamofire

class AuthService {
    static var message: String?
    
    static func postRequest(urlString: String, headers: HTTPHeaders, parameters: [String: Any]) {
        AF.request(urlString, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            let info = serializeResponse(data: response.data ?? Data())
            switch response.result {
            case .success(_):
                if info[APIConstants.isSuccess] as? Bool ?? false == true {
                    if info[APIConstants.data] != nil {
                        if urlString == APIConstants.baseURL + APIConstants.account + "login" {
                            let userData = info[APIConstants.data] as! [String: Any]
                            let role = userData[APIConstants.roles] as? [String] ?? [APIConstants.emptyValue]
                            if role.contains(APIConstants.partner) == true {
                                UserDefaults.standard.set(userData[APIConstants.token], forKey: APIConstants.token)
                                UserDefaults.standard.set(userData[APIConstants.refreshToken], forKey: APIConstants.refreshToken)
                                UserDefaults.standard.set(userData[APIConstants.email], forKey: APIConstants.emailAddress)
                                UserDefaults.standard.set(userData[APIConstants.firstname], forKey: APIConstants.firstName)
                                UserDefaults.standard.set(userData[APIConstants.id], forKey: APIConstants.userId)
                                NotificationCenter.default.post(name: Notification.Name(NOTIFICATION_REQUEST_SUCCESS), object: nil)
                            } else {
                                message = "You are not a registered partner"
                                NotificationCenter.default.post(name: Notification.Name(NOTIFICATION_REQUEST_FAILURE), object: nil)
                            }
                        } else if urlString == APIConstants.baseURL + APIConstants.account + "logout" {
                            UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.token)
                            UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.refreshToken)
                            UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.userId)
                            UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.restaurantSlug)
                            NotificationCenter.default.post(name: Notification.Name(NOTIFICATION_REQUEST_SUCCESS), object: nil)
                        } else if urlString == APIConstants.baseURL + "deals" {
                            let userData = info[APIConstants.data] as? String
                            message = userData
                            NotificationCenter.default.post(name: Notification.Name(NOTIFICATION_REQUEST_SUCCESS), object: nil)
                        }
                    }
                } else {
                    let userData = info[APIConstants.errorMessages] as? [String] ?? [APIConstants.somethingWrong]
                    message = userData.joined()
                    NotificationCenter.default.post(name: Notification.Name(NOTIFICATION_REQUEST_FAILURE), object: nil)
                }
            case .failure(let errorMsg):
                let response = response.debugDescription
                
                if response.contains(APIConstants.unauthorizedResponse) {
                    NotificationCenter.default.post(name: Notification.Name(NOTIFICATION_EXPIRED_TOKEN), object: nil)
                } else if response.contains(APIConstants.badResponse) {
                    UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.token)
                    UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.refreshToken)
                    UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.userId)
                    UserDefaults.standard.set(APIConstants.emptyValue, forKey: APIConstants.restaurantSlug)
                    
                    NotificationCenter.default.post(name: Notification.Name(NOTIFICATION_FORBIDDEN_REQUEST), object: nil)
                } else {
                    message = errorMsg.localizedDescription
                    NotificationCenter.default.post(name: Notification.Name(NOTIFICATION_SERVER_ERROR), object: nil)
                }
            }
        }
    }
    
    static func refreshUserToken(refreshToken: String, userId: String) {
        let parameters: [String: Any] = [
            APIConstants.refreshToken: refreshToken,
            APIConstants.userId: userId
        ]
        
        let headers: HTTPHeaders = [APIConstants.contentType: APIConstants.applicationJson]
        
        AF.request(APIConstants.baseURL + APIConstants.account + "refresh-token", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            
            if Connectivity.isConnected() { //Check connectivity status
                let info = serializeResponse(data: response.data ?? Data())
                
                switch response.result {
                case .success(_):
                    if info[APIConstants.isSuccess] as? Bool ?? false == true {
                        if info[APIConstants.data] != nil {
                            let userData = info[APIConstants.data] as! [String: Any]
                            UserDefaults.standard.set(userData[APIConstants.token], forKey: APIConstants.token)
                            UserDefaults.standard.set(userData[APIConstants.refreshToken], forKey: APIConstants.refreshToken)
                            print("Done...")
                        }
                    } else {
                        let userData = info[APIConstants.errorMessages] as? [String] ?? [APIConstants.somethingWrong]
                        message = userData.joined()
                        print(message ?? APIConstants.emptyValue)
                    }
                case .failure(_):
                    print("Refresh Token Error...")
                }
            } else {
                _ = APIConstants.emptyValue
            }
        }
    }
    
    static func newToken() {
        let refreshToken = UserDefaults.standard.object(forKey: APIConstants.refreshToken) as? String ?? APIConstants.emptyValue
        let userId = UserDefaults.standard.object(forKey: APIConstants.userId) as? String ?? APIConstants.emptyValue
        refreshUserToken(refreshToken: refreshToken, userId: userId)
    }
}

//public func refreshUserToken(refreshToken: String, userId: String) {
//    let parameters: [String: Any] = [
//        APIConstants.refreshToken: refreshToken,
//        APIConstants.userId: userId
//    ]
//
//    let headers: HTTPHeaders = [APIConstants.contentType: APIConstants.applicationJson]
//
//    AF.request(APIConstants.baseURL + APIConstants.account + "refresh-token", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
//
//        if Connectivity.isConnected() { //Check connectivity status
//            let info = serializeResponse(data: response.data ?? Data())
//
//            switch response.result {
//            case .success(_):
//                if info[APIConstants.isSuccess] as? Bool ?? false == true {
//                    if info[APIConstants.data] != nil {
//                        let userData = info[APIConstants.data] as! [String: Any]
//                        UserDefaults.standard.set(userData[APIConstants.token], forKey: APIConstants.token)
//                        UserDefaults.standard.set(userData[APIConstants.refreshToken], forKey: APIConstants.refreshToken)
//                        print("Done...")
//                    }
//                } else {
//                    let userData = info[APIConstants.errorMessages] as? [String] ?? [APIConstants.somethingWrong]
//                    let message = userData.joined()
//                    _ = message
//                    print(message)
//                }
//            case .failure(_):
//                _ = APIConstants.emptyValue
//                print("Refresh Token Error...")
//            }
//        } else {
//            _ = APIConstants.emptyValue
//        }
//    }
//}
//
//public func newToken() {
//    let refreshToken = UserDefaults.standard.object(forKey: APIConstants.refreshToken) as? String ?? APIConstants.emptyValue
//    let userId = UserDefaults.standard.object(forKey: APIConstants.userId) as? String ?? APIConstants.emptyValue
//    refreshUserToken(refreshToken: refreshToken, userId: userId)
//}

//    static func loginUser(email: String, password: String) {
        
//        let headers: HTTPHeaders = [APIConstants.contentType: APIConstants.applicationJson]
//
//        let parameters: [String: Any] = [
//            APIConstants.email: email,
//            APIConstants.password: password
//        ]
        
//        AF.request(APIConstants.baseURL + APIConstants.account + "login", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            
//            if Connectivity.isConnected() { //Check connectivity status
//                let info = serializeResponse(data: response.data ?? Data())
//                switch response.result {
//                case .success(_):
//                    if info[APIConstants.isSuccess] as? Bool ?? false == true {
//                        if info[APIConstants.data] != nil {
//                            let userData = info[APIConstants.data] as! [String: Any]
//                            let role = userData[APIConstants.roles] as? [String] ?? [APIConstants.emptyValue]
//
//                            if role.contains(APIConstants.partner) == true {
//                                UserDefaults.standard.set(userData[APIConstants.token], forKey: APIConstants.token)
//                                UserDefaults.standard.set(userData[APIConstants.refreshToken], forKey: APIConstants.refreshToken)
//                                UserDefaults.standard.set(userData[APIConstants.email], forKey: APIConstants.emailAddress)
//                                UserDefaults.standard.set(userData[APIConstants.firstname], forKey: APIConstants.firstName)
//                                UserDefaults.standard.set(userData[APIConstants.id], forKey: APIConstants.userId)
//                                NotificationCenter.default.post(name: Notification.Name(NOTIFICATION_LOGIN_SUCCESS), object: nil)
//                            } else {
//                                message = "You are not a registered partner"
//                                NotificationCenter.default.post(name: Notification.Name(NOTIFICATION_LOGIN_FAILURE), object: nil)
//                            }
//                        }
//                    } else {
//                        let userData = info[APIConstants.errorMessages] as? [String] ?? [APIConstants.somethingWrong]
//                        message = userData.joined()
//                        NotificationCenter.default.post(name: Notification.Name(NOTIFICATION_LOGIN_FAILURE), object: nil)
//                    }
//                case .failure(_):
//                    message = APIConstants.somethingWrong
//                    NotificationCenter.default.post(name: Notification.Name(NOTIFICATION_SERVER_ERROR), object: nil)
//                }
//            }
//        }
//    }
